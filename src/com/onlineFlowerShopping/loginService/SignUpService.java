package com.onlineFlowerShopping.loginService;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SignUpService
 */
@WebServlet("/SignUpService")
public class SignUpService extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	public static TreeMap<String, String> credsMap = new TreeMap<String, String>();
    public SignUpService() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String emailEntered = request.getParameter("email");
		String passwordEntered = request.getParameter("pwd");
		
		PrintWriter out = response.getWriter();
		
		if(credsMap.containsKey(emailEntered)){
			String htmlResponse = "<html>";
			htmlResponse += "<h2>You are an Exisiting Customer.....</h2>";
			htmlResponse += "</html>";
			out.println(htmlResponse);
		}
		else{
			credsMap.put(emailEntered, passwordEntered);
			String htmlResponse = "<html>";
			/*htmlResponse += "<h2>" + emailEntered + "</h2>";
			htmlResponse += "<h2>" + passwordEntered + "</h2>";*/
			htmlResponse += "<h2>" + credsMap.containsKey(emailEntered) + "</h2>";
			htmlResponse += "<h2>Signed Up Successfully!!!</h2>";
			htmlResponse += "</html>";
			out.println(htmlResponse);
		}
	}

}
