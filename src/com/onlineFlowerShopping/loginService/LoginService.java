package com.onlineFlowerShopping.loginService;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.TreeMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LoginService
 */
@WebServlet("/LoginService")
public class LoginService extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	
	
    public LoginService() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
/*		String emailEntered = request.getParameter("email");
		String password = request.getParameter("password");
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		if(credsMap.containsKey(emailEntered) && (credsMap.get(emailEntered)==password)){
			out.println("Logged in successfully");
			RequestDispatcher requestDispatcherObj = request.getRequestDispatcher("/login");
			requestDispatcherObj.include(request, response);
		}
		else{
			out.println("Sorry wrong credentials");
			RequestDispatcher requestDispatcherObj = request.getRequestDispatcher("/login");
			requestDispatcherObj.include(request, response);
		}*/
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String emailEntered = request.getParameter("email");
		String password = request.getParameter("password");
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		if(SignUpService.credsMap.containsKey(emailEntered) && (SignUpService.credsMap.get(emailEntered)==password)){
			out.println("Logged in successfully");			
		}
		else{
			out.println("Sorry wrong credentials");
		}
	}

}
