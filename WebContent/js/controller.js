var myApp = angular.module("myApp", ['ngRoute']);

myApp.controller("contactController", function($scope) {
	
	/*var value = document.getElementById("email").value;*/
	$scope.contactSubmitMessage = function(){
		/*console.log(value);*/
		if($scope.email != undefined && $scope.review != undefined){
			$scope.message="Thanks for contacting....We will get back to you soon";
		}
		else{
			$scope.message="Please fill the required fields!!!";
		}
		/*alert("into function");*/
		
		/*alert($scope.message);*/
	}
});

myApp.controller("signupController", function($scope,$http) {
	
	$scope.email;
	$scope.typedPwd;
	
	$scope.validityValEmail;
	$scope.validityValPwd;
	$scope.flag;
	
	$scope.validateEmail = function(){
		if($scope.email == undefined){
			$scope.validityValEmail = true;
		}
		else{
			var splitString = $scope.email.split("@");
			var splitStringByPeriod = splitString[1].split(".");
			if(splitStringByPeriod.length !=2){
				$scope.validityValEmail = true;
			}
			else{
				$scope.validityValEmail = false;
			}
		}
	}
	
	$scope.validatePwd = function(){
		var pwdPattern = /([a-zA-Z0-9]*[!@#$%^&*()]+)/; 
		if(!($scope.typedPwd.length>=8 && pwdPattern.test($scope.typedPwd))){
			$scope.validityValPwd = true;
		}
		else{
			$scope.validityValPwd = false;
		}
		
	}
	
	$scope.matchPassword = function(){
		if($scope.typedPwd == $scope.retypedPwd){
			$scope.flag = false;
		}
		else{
			$scope.flag = true;
		}
	}
	
	$scope.showPassword = function(){
		$scope.showPwd = document.getElementById("check").checked;
		var val = document.getElementById("pwd1");
		if($scope.showPwd){
			val.setAttribute('type', 'text');
		}
		else{
			val.setAttribute('type', 'password');
		}
	}
	
	$scope.submitForm = function(){
		$http.get("partials/SignUp.html")
			.then(function(response) {
				$scope.signUpMessage = "Signed up successfully";
				alert($scope.signUpMessage);
				
			}, function(response) {
				$scope.signUpMessage = response.status;
				alert($scope.signUpMessage);
			});
	}

});

myApp.controller("loginController", function($scope, $http) {
	$scope.email;
	$scope.password;

	$scope.submitForm = function(){
		$http.get("partials/Login.html")
		.then(function(response) {
			/*$scope.loginMessage = response.data; This gives the whole page that is loaded in the console*/ 
			$scope.loginMessage = "Logged in successfully";
			alert($scope.loginMessage);
			/*console.log($scope.loginMessage);*/
		}, function(response) {
			$scope.loginMessage = response.status;
			alert($scope.loginMessage);
			/*console.log($scope.loginMessage);*/
		});
	}
});

myApp.config(function($routeProvider){
	$routeProvider
		.when("/home",{
			templateUrl: "partials/Home.html",
			/*controller: "homeController"*/
		})
		.when("/shop",{
			templateUrl: "partials/Flower-Shopping.html",
			/*controller:"shopController"*/
		})
		.when("/signup",{
			templateUrl: "partials/SignUp.html",
			controller: "signupController"
		})
		.when("/contactus",{
			templateUrl: "partials/ContactUs.html",
			controller: "contactController"
		})
		.when("/login", {
			templateUrl: "partials/Login.html",
			controller: "loginController"
		})
		.when("/birthdayBouquets", {
			templateUrl: "partials/BirthdayBouquets.html"
		})
		.when("/anniversaryBouquets", {
			templateUrl: "partials/AnniversaryBouquets.html"
		})
		.when("/roses",{
			templateUrl: "partials/Roses.html"
		})
		.when("/tulips", {
			templateUrl: "partials/Tulips.html"
		})
		.when("/mixed", {
			templateUrl: "partials/Mixed.html"
		})
});
